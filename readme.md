![LOGO](documentation/img/umich.png)

## Table of Contents

### About

This repository is to document a ROS directed study class for Professor D. Daniszewski at the University of Michigan - Dearborn 

This directed study will focus on understanding the design, functionality, and structure of ROS and determine its feasibility for use on the nVidia Jetson TK1 embedded microprocessor board.  ROS is a collection of software libraries which abstract common tasks frequently encountered with autonomous systems.  These include determining position information, reading sensor data, and issuing control signals to motors and actuators.  

The goal of this directed study is to develop an understanding of the components that make up ROS and their interactions.  The dependencies of software modules will be identified and the tools needed to compile modules will be documented.  The design patterns used by ROS will be identified, such as publish/subscribe.  

The major areas of ROS to be studied are:
    1.  Sensor data acquisition and communication to microprocessor
    2.  Motor control messaging 
    3.  Localization and navigation ( SLAM, use of IMU, visual odometry )
    4.  Visualization of sensor input

A final report summarizing ROS and the interaction and dependencies of the libraries will be written and the core ROS code will be compiled and run on the Jetson TK1 microprocessor board.  All compiler options and compilation and linker flags will be documented.  Integrating a USB camera and processing of visual image data using ROS modules will be demonstrated and source code will be documented and stored in a repository such as GitHub.


### Quick Resources
* [JetsonHacks](http://www.jetsonhacks.com)
* [Ubuntu 14.04.5 (Trusty Tahr) Desktop Version 64-bit](http://releases.ubuntu.com/14.04/ubuntu-14.04.4-desktop-amd64.iso)
* [VirtualBox (Free Virutal Machine)](https://www.virtualbox.org/wiki/Downloads)
* Default LT4 Ubuntu Password: `ubuntu`
* [Indigo ROS Wiki](https://wiki.ros.org/indigo)
* Configure joy_node: rosparam set joy_node/dev "/dev/input/js2"

### Things you will need to complete Jetson TK1 setup:
* Jetson TK1 w/ Power Cable
* Host Linux Machine (best with Ubuntu 14.04.4 Trusty Tahr)
* Wired Internet Connection
* Micro USB Cable
* USB Keyboard
* USB/Serial Interface or HDMI Monitor
* Wired internet connection
* USB Expander (optional)

### Initial Setup

Before you start using your Jetson TK1 you will need to  perform the following:

#### 1. [Install L4T 21.2](documentation/setup/l4t.md)

This step must be performed on a host computer and requires a Micro USB cable. 

#### 2. [Install Grinch Kernel on Jetson TK1](documentation/setup/grinch.md)

Install the Grinch Kernel with updated features and drivers to your Jetson TK1

#### 3. [Install ROS on Jetson TK1](documentation/setup/ros.md)

Install ROS on your Jetson TK1

#### 4. [Configure ROS on Jetson TK1](documents/setup/configuration.md)

Some things you should do to configure ROS and your Jetson TK1

#### 5. [Install ROS on Ubuntu Trust Tahr for Desktop](documentation/setup/desktop.md)

How to get ROS running on a laptop or desktop computer

#### 6. [Initialize Catkin Workspace for Development](documentation/setup/catkin_workspace.md)

Setup your catkin workspace for development

### How-To References


#### [Recover Jetson TK1 to Latest L4T OS](documentation/how-to/recover.md)

Recover the Jetson TK1 or upgrade to the latest L4T version

#### [How to use git](documentation/how-to/git.md)

The basics that you will need to use git

#### [Setup Joystick Node](documentation/how-to/joy.md)

How to setup a joystick node in ROS

#### [Create example ROS package](documentation/how-to/create_package.md)

Create an example ROS package using catkin


## Sources

The following websites were used in the creating of this documentation:
* [https://github.com/e-lab/torch-toolbox/blob/master/Tutorials/Recover-filesystem-on-Jeson-TK1.md](https://github.com/e-lab/torch-toolbox/blob/master/Tutorials/Recover-filesystem-on-Jeson-TK1.md)
* [http://jetsonhacks.com/2015/05/26/install-grinch-kernel-for-l4t-21-3-on-nvidia-jetson-tk1/](http://jetsonhacks.com/2015/05/26/install-grinch-kernel-for-l4t-21-3-on-nvidia-jetson-tk1/)
* [http://jetsonhacks.com/2015/05/27/robot-operating-system-ros-on-nvidia-jetson-tk1/](http://jetsonhacks.com/2015/05/27/robot-operating-system-ros-on-nvidia-jetson-tk1/)

## Interesting Sites

* [MIT RACECAR Walkthrough – NVIDIA Jetson TK1](http://jetsonhacks.com/2015/10/06/mit-racecar-walkthrough-nvidia-jetson-tk1/)
* [Joystick Packag - Xbox Controller](http://wiki.ros.org/joy)

## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewsk
