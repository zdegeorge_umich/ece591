![LOGO](../img/umich.png)

### [Home](../../readme.md)

## About this document

This document is for seting up your catkin workspace so that you can develop your own ROS nodes and packages. 

## Initializing you catkin workspace

First you need to create the directory for the catkin workspace. You can do this in your home directory using the following commands:

~~~bash
$ mkdir ~/catkin_ws
$ mkdir ~/catkin_ws/src
~~~

Next switch to the catkin_ws directory and initialize the new workspace.

~~~bash
$ cd ~/catkin_ws/src
$ catkin_init_workspace
~~~

Build the workspace, even though there aren't any packages included yet. 

~~~bash
$ cd ~/catkin_ws
$ catkin make
~~~

Next make the workspace visible to ros by updating your environment variables with these commands:

~~~bash
$ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
$ source ~/.bashrc
~~~

Now you are setup to start creating your own catkin packages. 

## Source

Joseph, L. (2015). Mastering ROS for Robotics Programming. Birmingham, UK: Packt Publishing. 

Additional resources include [ROS Tutorials - Create a Workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace).

## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


