![LOGO](../img/umich.png)

### [Home](../../readme.md)

## About this document

This guide is for installing and setting up ROS on Ubuntu 14.04.5 (Trusty Tahr) using a virtual machine. These steps can also be done on a native installation of Ubuntu. 

## Resources

Please use the following resources for completing this walk-through. 

* [VirtualBox (Free Virutal Machine)](https://www.virtualbox.org/wiki/Downloads)
* [Ubuntu 14.04.5 (Trusty Tahr) Desktop Version 64-bit](http://releases.ubuntu.com/14.04/ubuntu-14.04.4-desktop-amd64.iso)

Going forward this tutorial assumes that you have Ubuntu 14.04.5 installed on a either a virtual machine or as a direct installation on a computer. 

## Seting up your desktop environment

After you have installed ubuntu on your machine follow these steps to set up your ROS environment.

### Install Git 

This first thing you should do is install git and update your credentials. Do this by running the following commands:

~~~bash
$ sudo apt-get update
$ sudo apt-get install git 
~~~

Then run the following commands with your personal information provided:

~~~bash
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
~~~

Now your git is setup and you will be able to use git for retrieving and modifying ROS packages. 

### Setup your Ubuntu Repositories for ROS

Run the following commands to setup your Ubuntu for ROS repositories:

~~~bash
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
~~~

Enter in your password. 

Then setup your keys by running the following command:

~~~bash
$ ssudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv-key 0xB01FA116
~~~

After update your packages again:

~~~bash
$ sudo apt-get update
~~~

### Install ROS

Install ros by running the following command:

~~~bash
$ sudo apt-get install ros-indigo-desktop-full
~~~

Note: This may take awhile depending on your internet connection. 


After installation you can find available Indigo packages by using the following command:

~~~bash
$ apt-cache search ros-indigo
~~~

Before ROS is usable you must also initialize rosdep, which is used for installing system dependencies for some components of ROS. Do this by running the following commands:

~~~bash
$ sudo rosdep init
$ rosdep update
~~~

### Setup Environment for ROS

Automatically add the ROS environment variables to every shell launch for convenience:

~~~bash
$ echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
$ source ~/.bashrc
~~~

Additionally, if you just want to add the environment variables to the current shell you are in you can use the following command:

~~~bash
$ source /opt/ros/indigo/setup.bash 
~~~

Note: You will have to restart your terminal for these changes to take effect.

### Test your setup

After you have sourced the setup.bash file for ROS and restarted your terminal you can test your setup by ensuring that the ROS environment variables are active by running the following command:

~~~bash
$ printenv | grep ROS
~~~

Your output should look something like this:

~~~bash
isc@isc-VirtualBox:~$ printenv | grep ROS
ROS_ROOT=/opt/ros/indigo/share/ros
ROS_PACKAGE_PATH=/opt/ros/indigo/share:/opt/ros/indigo/stacks
ROS_MASTER_URI=http://localhost:11311
ROSLISP_PACKAGE_DIRECTORIES=
ROS_DISTRO=indigo
ROS_ETC_DIR=/opt/ros/indigo/etc/ros
isc@isc-VirtualBox:~$
~~~

Additionally you can test your installing by ensuring that roscore runs correctly. Do this by running the following command:

~~~bash
$ roscore
~~~

You should see positive feedback. Terminate the task when complete by pressing 'ctrl-c'


## Source

The source of this content was derived from the following [website](https://wiki.ros.org/indigo). For more information on how to install ROS or about ROS please see there. 

## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


