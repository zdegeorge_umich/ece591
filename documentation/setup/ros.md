![LOGO](../img/umich.png)

### [Home](../../readme.md)

## Before You Install ROS

Before you install ROS on your Jetson TK1 you should ensure that you have installed the latest L4T operating system and installed the Grinch Kernel. If you have not done these steps please use the links below and follow the instructions before you continue to installing ROS. 
* [Install Latest L4T OS](l4t.md)
* [Install Grinch Kernel](grinch.md)

You also must have a reliable internet connection.

## Installing ROS

Again, thanks to the hard work of the people at [JetsonHacks](http://www.jetsonhacks.com), installing ROS on your Jetson TK1 takes no more than a few lines of commands. 

To proceed you must have `git` installed. If you have not installed git please do so at this time by performing the following commands:

~~~bash
$ sudo apt-get update
$ sudo apt-get install git
~~~

Next install ROS by running the following commands:

~~~bash
$ git clone https://github.com/jetsonhacks/installROS.git
$ cd installROS
$ ./installROS.h

This will install ROS Indigo ros-base, rosdep, and rosinstall.

You now have ROS installed.

## Source

The source of this content was derived from the following [website](http://jetsonhacks.com/2015/05/27/robot-operating-system-ros-on-nvidia-jetson-tk1/). For more information on how to install ROS or about ROS please see there. 

## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


