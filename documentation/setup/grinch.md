![LOGO](../img/umich.png)

### [Home](../../readme.md)

## Why Install Grinch

While the Jetson TK2 board comes loaded with tons of capability not all of the functionality of the hardware is enabled out of the box. Lucky for us there is the Grinch Kernel that adds support for an increased variety of peripherals as well as fixing some of the other known issues with the Jetson TK1 board. Installing the Grinch Kernel is one of the first things you should do with an out of the box Jetson Tk1. Thankfully the people at JetsonHacks have done all the hard work when it comes to installing Grinch, and so all you will have to do is download a repository from them and run a script. Thank you JetsonHacks!


## Before You Install Grinch

Before you install the Grinch Kernel you will want to have the latest version of the L4T OS on your Jetson TK1. In order to ensure that you have the latest OS you can follow the procedure at [Recover Your Jetson TK1](../how-to/recover.md). If you already know that you have the latest L4T OS on your Jetson TK1 board, or if you have already installed some version of the L4T OS, then you can proceed to installing the Grinch Kernel.


## Intalling Grinch

Thanks to the hard work of the people at [JetsonHacks](http://www.jetsonhacks.com) installing the Grinch Kernel will only take a few lines of commands in a terminal. 

First you must have `git` installed in order to proceed. To install `git` type the following commands:

~~~ bash
$ sudo apt-get update
$ sudo apt-get install git
~~~

To install the Grinch Kernel type the following commands:

~~~~ bash
$ git clone https://github.com/jetsonhacks/InstallGrinch.git
$ cd InstallGrinch
$ ./InstallGrinch.sh
~~~

After all the files have finished downloading you will be prompted to enter your password before the installation will complete. Note, that the default password for the L4T OS is `ubuntu` unless you have changed it. 

## Source

The source of this content was derived from the following [website](http://www.jetsonhacks.com/2015/05/26/install-grinch-kernel-for-l4t-21-3-on-nvidia-jetson-tk1/). For more information about how to install the Grinch Kernel or to learn more about the Grinch Kernel please see there. 


## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


