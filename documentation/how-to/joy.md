![LOGO](../img/umich.png)

### [Home](../../readme.md)

## About this document

This document is meant to guide you through setting up a joystick node in ROS. 

## What you will need

You will need:

	1. Ubuntu 14.04.5 Trusty Tahr installed on a computer (either virtual machine or native installation)
	2. ROS Ingigo installed on Ubuntu (see [Install ROS on Ubuntu Trust Tahr - Desktop](../setup/desktop.md))
	3. Xbox 360 remote with USB dongle

## Step 1. Install joy package

Install the joy package by running the following command:

~~~bash 
$ sudo apt-get install ros-indigo-joy
~~~

## Step 2. Attach joystick to machine and test connection

Attach the USB dongle to your computer.

If you are using a virtual machine you will need to make sure that it is connnected to the Ubuntu virtual machine. 

If you are using VirtualBox you can do this by bringing your curser to the top of your virtual machine and then selecting 'Devices->USB->Microsoft Xbox 360 Wireless Reciever for Windows'

Note: The wireless reciever should not need any special drivers for Ubuntu, and as soon as you connect it to the Ubuntu machine the light on the reciever should be lit and you should be able to connect your joystick to the reciever. 

Test your joystick by running the following command

~~~bash
$ ls /dev/input/
~~~

You should see 'js0' and possible additional 'js#' values in the output. Sample output is shown below:

~~~bash
isc@isc-VirtualBox:~/isc/ece591$ ls /dev/input
by-id    event0  event2  event4  event6  js1   mouse0
by-path  event1  event3  event5  js0     mice  mouse1
~~~

Make sure the joystick is working by running the following command (where js# is the joystick value):

~~~bash 
$ sudo jstest /dev/input/js#
~~~

Note: If there are more than one js# then you may need to try multiple ones to find the one that is your actual joystick. 

You should be not be able to see the output of the joystick on the terminal. Press some buttons and move some joysticks to ensure that your joystick is being read properly. 

## Step 3. Configure the joystick

You will need to make sure the the joystick is accessible to the ROS joy node. First check the permissions of the joystick by using the following command:

~~~bash
$ ls -l /dev/input/js#
~~~

You should see some out like this:

~~~bash
isc@isc-VirtualBox:~$ ls -l /dev/input/js2
crw-rw-r--+ 1 root root 13, 2 Nov 27 16:19 /dev/input/js2
~~~

Run the following command to update the perimssions of the joystick:

~~~bash
sudo chmod a+rw /dev/input/js#
~~~

Now re-run the previous command to verify that the permissions have been updated. Your output should look like this:

~~~bash
isc@isc-VirtualBox:~$ ls -l /dev/input/js2
crw-rw-rw-+ 1 root root 13, 2 Nov 27 16:19 /dev/input/js2
~~~

## Step 4. Test the joy node

Now that the joystick is connected and configured correctly we can test the node by using 'rostopic echo' command to see the output from the node. 

First open a new terminal window and run roscore by entering the following command:

~~~bash
$ roscore
~~~

Next open another terminal window and run the following command to configure the joy node to the propper js#. 

~~~bash
$ rosparam set joy_node/dev "/dev/input/js#"
~~~

In that same terminal you can start the joy node by running the following command:

~~~bash
$ rosrun joy joy_node
~~~

Your feedback should look like this:

~~~bash
isc-VirtualBox:~$ rosparam set joy_node/dev "/dev/input/js2"
isc@isc-VirtualBox:~$ rosrun joy joy_node
[ INFO] [1480282697.595207841]: Opened joystick: /dev/input/js2. deadzone_: 0.050000.
~~~

Now open another terminal and use 'rostopic' to debug the output from the node:

~~~bash
$ rostopic echo joy
~~~

You should now be able to provide input to the joystick and see the values through the rostopic node. 

## Source

The source of this content was derived from the following [website](http://wiki.ros.org/joy/Tutorials/ConfiguringALinuxJoystick). 

## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


