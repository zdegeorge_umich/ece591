![LOGO](../img/umich.png)

### [Home](../../readme.md

## About this document

This document will walk you through creating a new ROS package using catkin that will utilize both publishing and subscribing to internal and external messages through ROS. In this example we will be setting up a relay node to read input joystick data from the 'joy' package, and then this node will send a message to another node 'listener' if the 'A' button of the controller is being pressed. 

This toturial assumes that you have already setup your catkin workspace. If you have not done this yet please see the document [here](../setup/catkin_workspace.md).

Additionally you will need to have an Xbox 360 controller and USB dongle. For help on setting up the Xbox controller with the 'joy' package see the document [here](./joy.md).

## Step 1. Create new package using catkin

Create a new package by first navigating to the /src direcory of your catkin workspace. If you have followed the other tutorials in this project you can do this by running the following command:

~~~bash
$ cd ~/catkin_ws/src
~~~

Inside the /src directory run the following command to create the package 'test_package' with the 'roscpp' and 'joy' dependencies:

~~~bash
$ catkin_create_pkg test_package roscpp joy
~~~

This command will make a file structure inside the /src directory for your new package test_package. Before we can complete the package we must now configure some files inside here. 

## Step 2. Configure your package

Navigate to your new package and list the contents. It should look like this:

~~~bash
isc@isc-VirtualBox:~/catkin_ws/src/test_package$ ls
CMakeLists.txt  include  package.xml  src
~~~

The CMakeLists.txt and package.xml files are used to configure your package. We will modify these files for this example. 

Open the package.xml file in a text editor and copy in the following contents:

~~~xml
<?xml version="1.0"?>
<package>
  <name>test_package</name>
  <version>0.0.1</version>
  <description>This is an example package</description>

  <maintainer email="isc@todo.todo">isc</maintainer>

  <license>BSD</license>

  <buildtool_depend>catkin</buildtool_depend>
  <build_depend>joy</build_depend>
  <build_depend>roscpp</build_depend>
  <run_depend>joy</run_depend>
  <run_depend>roscpp</run_depend>

</package>
~~~

Next open CMakeLists.txt and copy in the following contents:

~~~text
cmake_minimum_required(VERSION 2.8.3)
project(test_package)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS roscpp joy)

## Declare a catkin package
catkin_package()

## Build talker and listener
include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})

add_executable(relay src/relay.cpp)
target_link_libraries(relay ${catkin_LIBRARIES})
~~~

## Step 3. Create the source files

You will now need to add the files relay.cpp and listener.cpp to the /src directory of your package. Do this and then copy the following into the files:

relay.cpp
~~~c
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Joy.h"

#include <sstream>

#define BUTTON_A joy->buttons[0]
#define BUTTON_B joy->buttons[1]
#define BUTTON_X joy->buttons[2]
#define BUTTON_Y joy->buttons[3]
#define BUTTON_LB joy->buttons[4]
#define BUTTON_RB joy->buttons[5]
#define BUTTON_START joy->buttons[7]
#define BUTTON_POWER joy->buttons[8]
#define BUTTON_LS joy->buttons[9]
#define BUTTON_RS joy->buttons[10]

#define AXIS_L_LR joy->axes[0]
#define AXIS_L_UD joy->axes[1]
#define AXIS_R_LR joy->axes[2]
#define AXIS_R_UD joy->axes[3]
#define AXIS_TRIGGER joy->axes[4]
#define AXIS_CROSS_LR joy->axes[6]
#define AXIS_CROSS_UD joy->axes[7]

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */

ros::Publisher chatter_pub;

void chatterCallback(const sensor_msgs::Joy::ConstPtr& joy)
{	
	if(BUTTON_A)
	{
	    std_msgs::String msg;

	    std::stringstream ss;
	    ss << "The \'A\' Button has been pressed";
	    msg.data = ss.str();

	    ROS_INFO("%s", msg.data.c_str());

	    chatter_pub.publish(msg);
	}
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "talker");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;


  chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
  ros::Subscriber sub = n.subscribe("joy", 1000, chatterCallback);

  ros::spin();

  return 0;
}
~~~

listener.cpp
~~~c
#include "ros/ros.h"
#include "std_msgs/String.h"


/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "listener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}
~~~


#### Step 3. Build package

Your package is now complete. Build your package by navigating to the /catkin_ws directory and running the following command:

~~~bash
$ cd ~/catkin_ws
$ catkin_make
~~~

You now have a package 'test_package' with the nodes 'listener' and 'relay' as available nodes. We can now use these nodes to interact with the 'joy_node'


#### Step 4. Using roslaunch

If you were to start this system manually it would involve opening multiple terminals and typing many commands, so the smart people of ROS have developed a tool to help with launching topics, or groups of nodes. If you want do it the old fashioned way you could:

Open a terminal and type the following command:
~~~bash
$ roscore
~~~

Next open a new terminal and run the 'listener' node by running the following command:
~~~bash
$ rosrun test_package listener
~~~

Then open another terminal and run the 'relay' node by running the following command:
~~~bash
$ rosrun test_package listener
~~~

And finally open another terminal and run the 'joy_node' by running the following command:
~~~bash
$ rosrun joy joy_node
~~~

But by utilizing a launch file we can cut this work down. First run the following commands to create the launch directory and the launch file:
~~~bash
$ mkdir ~/catkin_ws/src/test_package/launch
$ touch ~/catkin_ws/src/test_package/launch/demo_topic.launch
~~~

Inside a text editor of you choice open the newly create demo_topic.launch file and paste in the following:
~~~xml
<launch>

	<node name="joy_node" pkg="joy" type="joy_node" />

	<node name="relay" pkg="test_package" type="relay" />

	<node name="listener" pkg="test_package" type="listener" output="screen"/>

</launch>
~~~

Now from anywhere you can launch your entire application in one terminal, including roscore, by using the following command:
~~~bash
$ roslaunch test_package demo_topic.launch
~~~

Try it!


## Author

Zachary DeGeorge | ECE 591
Prof. D. Daniszewski


